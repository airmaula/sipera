<?php

namespace Database\Seeders;

use App\Models\Activity;
use App\Models\Product;
use App\Models\Room;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $faker = Factory::create('id-ID');

        foreach (range(1, 5) as $item) {
            Room::create([
                'name' => 'Room ' . $item,
                'description' => $faker->numberBetween(50, 200) . ' rooms',
            ]);
        }

        $users = [
            ['name' => 'Admin', 'username' => 'admin', 'password' => bcrypt('admin'), 'position' => 'Staff', 'role' => 'admin'],
            ['name' => 'Cahya', 'username' => 'cahya', 'password' => bcrypt('cahya'), 'position' => 'Staff', 'role' => 'operator'],
            ['name' => 'Pepen', 'username' => 'pepen', 'password' => bcrypt('pepen'), 'position' => 'Wakasek', 'role' => 'officer'],
            ['name' => 'Dedi', 'username' => 'dedi', 'password' => bcrypt('dedi'), 'position' => 'Wakasek', 'role' => 'teacher'],
        ];

        foreach ($users as $user) {
            User::create($user);
        }

        $date = date('Y-m-d');
        $activity = Activity::create([
            'event_name' => 'Sosialisasi',
           'room_id' => 1,
           'date' => $date,
            'start' => '08:00:00',
            'end' => '10:00:00',
            'user_id' => 4,
            'approved_by' => 3,
            'status' => 'approved',
        ]);

        $products = [
            ['name' => 'Laptop', 'stock' => 20],
        ];

        foreach ($products as $product) {
            Product::create($product);
        }

        $activity->activity_products()->create([
           'product_id' => 1,
           'amount_of_products' => 10,
        ]);

    }
}
