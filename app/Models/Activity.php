<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    public function room() {
        return $this->belongsTo(Room::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function approved() {
        return $this->belongsTo(User::class, 'approved_by', 'id');
    }

    public function activity_products() {
        return $this->hasMany(ActivityProduct::class);
    }

}
