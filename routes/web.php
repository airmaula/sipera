<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    // ruangan
    $rooms = \App\Models\Room::all();

    return view('admin', compact('rooms'));
});


Route::match(['get', 'post'], '/room/{room}', function ($room_id) {

    date_default_timezone_set('Asia/Jakarta');

    // hari ini
    $now = date('Y-m-d');

    // aktifitas hari ini
    $activities = \App\Models\Activity::whereDate('date', $now)->where('room_id', $room_id)->get();

    if (request()->isMethod('post')) {

        $start = request()->start . ':00';
        $end = request()->end . ':00';

        // validasi waktu minimal
        $waktu_sekarang = date('H:i:s');
        $start_todate = date_create($start);
        $waktu_sekarang_todate = date_create($waktu_sekarang);
        $date_diff = date_diff($start_todate, $waktu_sekarang_todate);
        $minutes = ($date_diff->h * 60) + $date_diff->i;


        if ($minutes < 60) {
            echo "minimal 1 jam lah goblok!";
            return;
        }

        // validasi waktu & ruangan
        foreach ($activities as $activity) {
            if (($activity->start < $start && $activity->end > $start || $activity->start < $end && $activity->end > $end) && $activity->status === 'approved') {
                echo $activity->event_name;
                return;
            }
        }

        echo "Tidak ada di jam tersebut";
        return;
    }

    return view('activity', compact('activities'));
});
