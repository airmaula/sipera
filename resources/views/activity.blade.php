<form action="" method="POST">
    @csrf
    <label for="">Start</label>
    <input type="time" name="start">
    <br>
    <br>
    <label for="">End</label>
    <input type="time" name="end">
    <br>
    <br>
    <button type="submit">Request</button>
    <br><br>
</form>

<table border="1">
    <tr>
        <td>Nama Kegiatan</td>
        <td>Ruangan</td>
        <td>Mulai</td>
        <td>Selesai</td>
        <td>Peminjam</td>
        <td>Status</td>
        <td>Disetujui Oleh</td>
        <td>Barang yang dipinjam</td>
        <td>Dengan Internet</td>
    </tr>
    @foreach($activities as $activity)
        <tr>
            <td>{{ $activity->event_name }}</td>
            <td>{{ $activity->room->name }}</td>
            <td>{{ $activity->start }}</td>
            <td>{{ $activity->end }}</td>
            <td>{{ $activity->user->name }}</td>
            <td>{{ $activity->status }}</td>
            <td>{{ $activity->status === 'approved' ? $activity->approved->name : '-' }}</td>
            <td>
                <table border="1">
                    <tr>
                        <td>Nama Barang</td>
                        <td>Jumlah</td>
                    </tr>
                    @foreach($activity->activity_products as $ap)
                        <tr>
                            <td>{{ $ap->product->name }}</td>
                            <td>{{ $ap->amount_of_products }}</td>
                        </tr>
                    @endforeach
                </table>
            </td>
            <td>{{ $activity->with_internet ? 'Ya' : 'Tidak' }}</td>
        </tr>
    @endforeach
</table>


<br>
<br>
<h1>Barang</h1>
<table border="2">
    <tr>
        <td>Nama</td>
        <td>Stock</td>
    </tr>

    @foreach(\App\Models\Product::all() as $product)
        <tr>
            <td>{{$product->name}}</td>

            <?php
                $activities = [];

                foreach (\App\Models\Activity::all() as $activity) {
                    if ($activity->activity_products()->where('product_id', $product->id)->first() && $activity->status === 'approved') {
                        $activities[] = $activity;
                    }
                }

                $product_borrowed = 0;
                foreach ($activities as $activity) {
                    $ap = $activity->activity_products()->where('product_id', $product->id)->first();
                    if ($ap->product_id === $product->id) {
                        $product_borrowed += $ap->amount_of_products;
                    }
                }
            ?>
            <td>{{$product->stock - $product_borrowed}}</td>
        </tr>
    @endforeach
</table>
