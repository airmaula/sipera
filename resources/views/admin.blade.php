<table border="1">
    <tr>
        <td>Nama Ruangan</td>
        <td>Aksi</td>
    </tr>
    @foreach($rooms as $room)
        <tr>
            <td>{{ $room->name }}</td>
            <td><a href="{{ url('/room/' . $room->id) }}">Lihat</a></td>
        </tr>
    @endforeach
</table>
